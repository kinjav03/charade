use lib::entity::{Actor, Entity, InitActor, load_dictionary};
use lib::input::*;
use std::path::Path;
use std::collections::HashMap;
use tcod::colors::*;
use tcod::console::*;
use tcod::input::*;
use tcod::system::*;

mod lib {
    pub mod input;
    pub mod entity;
    pub mod render;
}

fn main() {
	// Read entity dictionaries and load their contents into initializer pools
    let mut initpool_actor: HashMap<String, InitActor> = HashMap::new();

    load_dictionary(Path::new("./data/dict_actors.yml"), &mut initpool_actor)
        .expect("Error loading dictionary");

    // Initialize the player and any test entities
    let mut player: Actor = Entity::init(3, 3, String::from("player"), &mut initpool_actor);

    let entity_list: Vec<Actor> = vec!(
        Entity::init(3, 6, String::from("goblin"), &mut initpool_actor),
        Entity::init(6, 3, String::from("orc"), &mut initpool_actor)
    );

    let mut root_console = Root::initializer()
        .font("terminal.png", FontLayout::AsciiInRow)
        .size(60, 40)
        .title("Charade - Pre-Alpha")
        .init();

    set_fps(60);

    // Main game loop
    while !root_console.window_closed() {
        root_console.set_default_foreground(player.color);
        root_console.set_default_background(BLACK);

        root_console.clear();

        root_console.put_char_ex(
            player.x_position as i32,
            player.y_position as i32,
            player.glyph,
            player.color,
            BLACK
        );

        for entity in &entity_list {
            root_console.put_char_ex(
                entity.x_position as i32,
                entity.y_position as i32,
                entity.glyph,
                entity.color,
                BLACK
            )
        }

        root_console.flush();

        let player_input = root_console.wait_for_keypress(true);
        match player_input.code {
            KeyCode::Escape => break,
            KeyCode::Up => process_movement(0, -1, &mut player),
            KeyCode::Down => process_movement(0, 1, &mut player),
            KeyCode::Left => process_movement(-1, 0, &mut player),
            KeyCode::Right => process_movement(1, 0, &mut player),

            _ => {}
        }
    }
}
