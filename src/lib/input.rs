use super::entity::Actor;

pub fn process_movement(x_move: i32, y_move: i32, entity: &mut Actor) {
	entity.x_position+=x_move;
	entity.y_position+=y_move;
}