use serde::Deserialize;
use std::path::Path;
use std::collections::HashMap;
use std::fs;
use tcod::colors::*;

pub trait Entity {
	// Initialize an entity from the given initializer
	fn init(
		x: i32, 
		y: i32, 
		internal_name: String, 
		init_dictionary: &mut HashMap<String, InitActor>
	) -> Self;	
}

/*
 An Actor is an entity that is capable of performing actions.
*/ 

// Structure Definition
pub struct Actor {
	pub x_position: i32,
	pub y_position: i32,
	pub name: String,
	pub glyph: char,
	pub color: Color,
	pub stats: ActorStats,
	pub traits: ActorTraits,
	pub inventory: Option<Vec<Box<dyn Item>>>,
	pub tags: Vec<String>
}

pub struct ActorStats {
	pub max_hp: i32,
	pub hp: i32,
}

pub struct ActorTraits {
	pub ai: String,
	pub alignment: String,
	pub size: String
}

// Initializer Definition
#[derive(Deserialize)]
pub struct InitActor {
	pub name: String,
	pub glyph: char,
	pub color: [u8; 3],
	pub inventory_size: usize,
	pub tags: Vec<String>,
	pub stats: InitActorStats,
	pub traits: InitActorTraits,
}

#[derive(Deserialize)]
pub struct InitActorStats {
	pub max_hp: i32
}

#[derive(Deserialize)]
pub struct InitActorTraits {
	pub ai: String,
	pub alignment: String,
	pub size: String
}

// Implement Block
impl Entity for Actor {
	fn init(x: i32, y: i32, internal_name: String, init_dictionary: &mut HashMap<String, InitActor>) -> Actor {
		let initializer = init_dictionary.get(&internal_name).unwrap();

		Actor {
			x_position: x,
			y_position: y,

			name: (*initializer.name).to_string(),
			glyph: initializer.glyph,
			color: Color::new(
				initializer.color[0],
				initializer.color[1],
				initializer.color[2],
			),

			inventory: match initializer.inventory_size {
				0 => None,
				_ => Some(Vec::with_capacity(initializer.inventory_size))
			},

			stats: ActorStats {
				max_hp: initializer.stats.max_hp,
				hp: initializer.stats.max_hp
			},

			traits: ActorTraits {
				ai: (*initializer.traits.ai).to_string(),
				alignment: (*initializer.traits.alignment).to_string(),
				size: (*initializer.traits.size).to_string()
			},

			tags: (*initializer.tags).to_vec(),
		}
	}
}

/*
 Items are a broader category of Entity, falling into one of two sub-categories:
 	- Equipables, which can be worn, wielded or otherwise equipped by a capable Actor.
 	- Evokables, which can cause a wide variety of effects when used by a capable Actor.
*/

pub trait Item {}

pub struct Equipable {}

impl Item for Equipable {}

pub struct Evokable {}

impl Item for Evokable {}


// Reads data from an entity dictionary (see the `data` directory) and loads it into an initializer pool
pub fn load_dictionary(dict_path: &Path, initializer_list: &mut HashMap<String, InitActor>) -> Result<(), std::io::Error> {
	let dict_string = &fs::read_to_string(dict_path)?;

	let dict_contents: Vec<serde_yaml::Value> = serde_yaml::from_str(dict_string).unwrap();

	println!("{:?}", dict_contents[0].get("stats"));

	for entry in dict_contents {
		let new_initializer: InitActor = serde_yaml::from_value(entry).unwrap();
		initializer_list.insert(new_initializer.name.to_string(), new_initializer);
	}



	Ok(())
}