# Charade Pre-Alpha 0.2.1

Charade is an old-school ASCII roguelike where the player takes on the role of a traditionally "evil" fantasy trope. Necromancers, warlocks, vampires and more take the stage in Charade, each with a unique style of play.

Here's some of what Charade has to offer:
- Enemies with personality types that spice up each encounter, affecting their stats and behavior.
- A creature sizing system which allows multiple creatures to take up one tile, or one creature to take up multiple tiles, depending on creature size.
- Player classes with unique progression, mechanics, and flair.

There's very little to see here, as the game has just begun development!